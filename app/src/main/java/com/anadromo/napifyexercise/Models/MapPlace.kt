package com.anadromo.napifyexercise.Models

import com.google.android.gms.maps.model.LatLng

class MapPlace(
        private val _placeName: CharSequence?,
        private val _placeAddress: String?,
        private val _placeAttrs: String?,
        private val _placeLatLng: LatLng?
) {

    var placeName = this._placeName ?: ""
    var placeAddress = this._placeAddress ?: ""
    var placeAttrs = this._placeAttrs ?: ""
    var placeLatLng = this._placeLatLng ?: LatLng(0.0,0.0)
}