package com.anadromo.napifyexercise.Models

class GiftCard(private val _title:String?,
               private val _message:String?) {

    val title get() = this._title ?: "No title"
    val message get() = this._message ?: "No message"
}