package com.anadromo.napifyexercise

class Constants {
    companion object {
        val DEFAULT_ZOOM = 14.0f
        val MAX_COUNT_PLACES = 5
        val ACCESS_FINE_LOCATION_CODE = 666
    }
}