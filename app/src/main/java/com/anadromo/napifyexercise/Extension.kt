package com.anadromo.napifyexercise

import android.support.design.widget.Snackbar

import android.view.View
import org.apache.commons.lang3.text.WordUtils
import java.util.*

fun View.showShortMessage(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}
fun ClosedRange<Int>.random() =
        Random().nextInt((endInclusive + 1) - start) +  start

fun CharSequence.capitalize(): String = WordUtils.capitalize(this.toString().toLowerCase())

