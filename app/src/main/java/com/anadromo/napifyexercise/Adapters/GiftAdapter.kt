package com.anadromo.napifyexercise.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.anadromo.napifyexercise.Models.GiftCard
import com.anadromo.napifyexercise.R
import com.anadromo.napifyexercise.showShortMessage

class GiftAdapter(val context: Context): RecyclerView.Adapter<GiftAdapter.ViewHolder>() {

    val layoutInflater by lazy{
        LayoutInflater.from(context)
    }

    var cardList = listOf<GiftCard>()

    fun updateAdapterList(cardList: List<GiftCard>) {
        this.cardList = cardList
        this.notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.gifts_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = cardList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val giftCard = this.cardList[position]
        holder.itemView.setOnClickListener {
            it.showShortMessage("Title -> ${giftCard.title}\nMessage ->${giftCard.message}")
        }
    }

    class ViewHolder(view: View?):RecyclerView.ViewHolder(view){
        val ivCardImage: ImageView? = view?.findViewById(R.id.ivCardImage)
    }
}