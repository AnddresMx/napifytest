package com.anadromo.napifyexercise.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class PageAdapter(private val manager : FragmentManager) : FragmentStatePagerAdapter(manager) {


    private val mFragmentList = ArrayList<Fragment>()
    private val mPageTitleList = ArrayList<String>()

    fun addFragment(fragment: Fragment, pageTitle: String): Unit {
        mFragmentList.add(fragment)
        mPageTitleList.add(pageTitle)
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getPageTitle(position: Int): String {
        return mPageTitleList[position]
    }
}
