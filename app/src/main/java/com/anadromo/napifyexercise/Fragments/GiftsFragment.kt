package com.anadromo.napifyexercise.Fragments


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anadromo.napifyexercise.Adapters.PageAdapter
import com.anadromo.napifyexercise.BuildConfig

import com.anadromo.napifyexercise.R
import kotlinx.android.synthetic.main.fragment_gifts.*


class GiftsFragment : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance() = GiftsFragment()
    }

    private val pageAdapter by lazy {
        PageAdapter(this.childFragmentManager)
    }
    private val fragmentsMap: MutableMap<String, Fragment> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentsMap["THANK YOU"] = GiftsThankYouFragment.newInstance()
        fragmentsMap["BIRTHDAY"] = GiftsBirthdayFragment.newInstance()
        fragmentsMap["LOVE"] = GiftsLoveFragment.newInstance()
        fragmentsMap["WORDS"] = GiftsWordsFragment.newInstance()

        for (element in fragmentsMap){
            pageAdapter.addFragment(element.value,element.key)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gifts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTabLayoutViewPager()
    }



    private fun setTabLayoutViewPager(){
        viewPager.adapter = pageAdapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab?.position!!
            }
        })
        tabLayout.setupWithViewPager(viewPager)

    }


}
