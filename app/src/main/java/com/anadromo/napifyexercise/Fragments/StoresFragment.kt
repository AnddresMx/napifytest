package com.anadromo.napifyexercise.Fragments

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.annotation.IntegerRes
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.RelativeLayout
import com.anadromo.napifyexercise.Constants
import com.anadromo.napifyexercise.Models.MapPlace

import com.anadromo.napifyexercise.R
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.stores_place_info_layout.view.*

class StoresFragment :
        Fragment(),
        OnMapReadyCallback {

    companion object {
        @JvmStatic
        fun newInstance() = StoresFragment()
    }

    private var isLocationPermissionGranted = false
    private val supportMapFragment: SupportMapFragment by lazy {
        this.childFragmentManager.findFragmentById(R.id.mapContainer) as SupportMapFragment
    }
    private val geoDataClient: GeoDataClient by lazy {
        Places.getGeoDataClient(this.activity!!, null)
    }
    private val placeDetectionClient: PlaceDetectionClient by lazy {
        Places.getPlaceDetectionClient(this.activity!!, null)
    }
    private val fusedLocationProvider by lazy {
        LocationServices.getFusedLocationProviderClient(this.activity!!)
    }
    private val placesList by lazy {
        mutableListOf<MapPlace>()
    }
    private var lastKnownLocation: Location? = null
    private lateinit var lastLatLngKnownLocation: LatLng

    private var nMap: GoogleMap? = null
    private lateinit var mapView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_stores, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMap()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_stores, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.current_place_action -> {
                showCurrentPlace()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        isLocationPermissionGranted = false
        when (requestCode) {
            Constants.ACCESS_FINE_LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isLocationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    override fun onMapReady(nMap: GoogleMap?) {
        this.nMap = nMap!!
        this.nMap?.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker?): View {
                val infoWindow: View = LayoutInflater.from(this@StoresFragment.activity!!).inflate(
                        R.layout.stores_place_info_layout,
                        null
                )

                infoWindow.title.text = marker?.title ?: "Not title found"
                infoWindow.snippet.text = marker?.snippet ?: "Not description found"
                return infoWindow
            }

            override fun getInfoWindow(marker: Marker?): View? {
                return null
            }
        })

        updateLocationUI()
        getDeviceLocation()
    }


    private fun setMap() {
        this.supportMapFragment.retainInstance = true
        this.supportMapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    private fun updateLocationUI() {
        if (isLocationPermissionGranted) {

            nMap?.isMyLocationEnabled = true
            nMap?.uiSettings?.isMyLocationButtonEnabled = true
            setLocationButtonPosition()

        } else {
            nMap?.isMyLocationEnabled = false
            nMap?.uiSettings?.isMyLocationButtonEnabled = false
            getLocationPermission()
        }
    }

    private fun setLocationButtonPosition(){
        mapView = this.view!!
        val locationButton:View = (((mapView.findViewById(Integer.parseInt("1"))) as View).parent as View).findViewById(Integer.parseInt("2"))
        val buttonLayoutParams = locationButton.layoutParams as RelativeLayout.LayoutParams
        buttonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP,0)
        buttonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE)
        buttonLayoutParams.setMargins(0,0,30,30)
    }


    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        if (isLocationPermissionGranted) {
            val locationResult = fusedLocationProvider.lastLocation
            locationResult.addOnCompleteListener({
                if (it.isSuccessful) {
                    lastKnownLocation = it.result

                } else {
                    nMap?.uiSettings?.isMyLocationButtonEnabled = false
                }

                lastLatLngKnownLocation = LatLng(
                        lastKnownLocation?.latitude!!,
                        lastKnownLocation?.longitude!!
                )

                nMap?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                                lastLatLngKnownLocation,
                                Constants.DEFAULT_ZOOM
                        )
                )
            })




        }
    }

    @SuppressLint("MissingPermission")
    private fun showCurrentPlace() {
        placesList.clear()
        if (isLocationPermissionGranted) {
            val placeResult =
                    placeDetectionClient.getCurrentPlace(null)
            placeResult.addOnCompleteListener({
                if (it.isSuccessful && it.result != null) {
                    val likelyPlaces = it.result
                    val count = if (likelyPlaces.count < Constants.MAX_COUNT_PLACES)
                        likelyPlaces.count
                    else Constants.MAX_COUNT_PLACES

                    for (likelyPlace in likelyPlaces) {
                        placesList.add(
                                MapPlace(
                                        likelyPlace.place.name,
                                        likelyPlace.place.address.toString(),
                                        likelyPlace.place.attributions.toString(),
                                        likelyPlace.place.latLng
                                )
                        )
                        if (placesList.size == count) break
                    }
                    likelyPlaces.release()
                }
                openPlacesDialog()
            })

        } else {
            getLocationPermission()
        }
    }

    fun openPlacesDialog() {
        val dialogListener = DialogInterface.OnClickListener { dialog, which ->
            nMap?.clear()
            nMap?.addMarker(
                    MarkerOptions()
                            .title(placesList[which].placeName.toString())
                            .snippet("${placesList[which].placeAddress} ${placesList[which].placeAttrs}")
                            .position(placesList[which].placeLatLng)
            )
            nMap?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                            placesList[which].placeLatLng,
                            14.0f
                    )
            )
        }

        val items: Array<CharSequence> = Array(placesList.size, { "" })
        for (i in placesList.indices) {
            items[i] = placesList[i].placeName
        }

        val dialog = AlertDialog.Builder(this.activity!!)

        if (items.isNotEmpty()) {
            dialog.setTitle(getString(R.string.stores_chose_place_legend))
                    .setItems(items, dialogListener)
        } else {
            dialog.setTitle(getString(R.string.stores_no_place_legend))
                    .setMessage(getString(R.string.stores_no_place_dialog_message))
                    .setNeutralButton(android.R.string.ok) { dialog, _ ->
                        dialog.dismiss()
                    }
        }

        dialog.show()

    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) {
            isLocationPermissionGranted = true
            updateLocationUI()
        } else {
            ActivityCompat.requestPermissions(
                    this.activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    Constants.ACCESS_FINE_LOCATION_CODE
            )
        }
    }
}
