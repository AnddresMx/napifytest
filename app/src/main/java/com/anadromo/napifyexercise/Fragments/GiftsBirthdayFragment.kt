package com.anadromo.napifyexercise.Fragments


import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_gifts_layout.view.*


class GiftsBirthdayFragment : BaseGiftFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = GiftsBirthdayFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.updateAdapterList(getCardList("Birthday"))
        view.rvGifts.adapter = this.adapter

    }





}
