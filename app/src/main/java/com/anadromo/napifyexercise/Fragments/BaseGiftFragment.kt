package com.anadromo.napifyexercise.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anadromo.napifyexercise.Adapters.GiftAdapter
import com.anadromo.napifyexercise.Models.GiftCard
import com.anadromo.napifyexercise.R
import com.anadromo.napifyexercise.random


 open class BaseGiftFragment : Fragment() {

    val adapter: GiftAdapter by lazy {
        GiftAdapter(this.context!!)
    }

     override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
         return inflater.inflate(R.layout.fragment_gifts_layout, container, false)
     }

    fun getCardList(cardType: String): List<GiftCard>{
        val cardList = mutableListOf<GiftCard>()
        val numberOfCards = (1..10).random()
        for(i in 0..numberOfCards){
            cardList.add(GiftCard("Card #${i+1}", "I'm $cardType card #${i+1}") )
        }

        return cardList
    }
}
