package com.anadromo.napifyexercise.Fragments


import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_gifts_layout.*


class GiftsThankYouFragment : BaseGiftFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = GiftsThankYouFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter.updateAdapterList(getCardList("ThankYou"))

        rvGifts.adapter = this.adapter

    }



}
