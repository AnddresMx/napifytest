package com.anadromo.napifyexercise.Fragments


import android.os.Bundle

import android.view.View

import kotlinx.android.synthetic.main.fragment_gifts_layout.view.*


class GiftsWordsFragment : BaseGiftFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = GiftsWordsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.updateAdapterList(getCardList("Words"))

       view.rvGifts.adapter = this.adapter

    }


}
