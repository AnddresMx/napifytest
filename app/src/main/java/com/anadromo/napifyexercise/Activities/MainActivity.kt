package com.anadromo.napifyexercise.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.view.MenuItem
import com.anadromo.napifyexercise.Fragments.*
import com.anadromo.napifyexercise.R
import com.anadromo.napifyexercise.capitalize
import com.anadromo.napifyexercise.showShortMessage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_layout.*

class MainActivity : AppCompatActivity() {
    private lateinit var drawerOptions:MutableMap<String, Fragment>
    private val fragmentMan by lazy {
        this.supportFragmentManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawerOptions = mutableMapOf(
                "Pay" to PayFragment.newInstance(),
                "Gifts" to GiftsFragment.newInstance(),
                "Stores" to StoresFragment.newInstance()
        )

        setToolbar()
        setDrawerLayout()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.END)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    fun setDrawerLayout(){
        navigation_view.setNavigationItemSelectedListener { selectedItem ->

            when(selectedItem.itemId){
                R.id.pay_menu_action,
                R.id.stores_menu_action,
                R.id.gift_menu_action  -> replaceFragment(selectedItem)

                R.id.menu_action,
                R.id.rewards_menu_action,
                R.id.messages_menu_action,
                R.id.settings_menu_action ->
                    navigation_view.showShortMessage("${selectedItem.title.capitalize()} section needs to be loaded here :)")

            }

            drawer_layout.closeDrawer(GravityCompat.START)

            true
        }
    }

    fun replaceFragment(selectedItem: MenuItem){

        val activityTitle = selectedItem.title.capitalize()

        val fragment = drawerOptions.get(activityTitle)

        fragmentMan.beginTransaction()
                .replace(R.id.fragment_container, fragment, activityTitle)
                .addToBackStack(activityTitle)
                .commit()

        supportActionBar?.title = activityTitle

        selectedItem.isChecked = true
    }

    fun setToolbar(){
        setSupportActionBar(app_bar)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_drawer_menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


}
